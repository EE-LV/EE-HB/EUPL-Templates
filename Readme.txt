This project provides LabVIEW (2010) templates that contain the European Public Licence (EUPL).
The extension for GPL v3 need to be added.

It still need to be agreed by the GSI management to use the EUPL for GSI open source developments.
